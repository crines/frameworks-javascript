'use strict'
var validator = require('validator');
var fs = require('fs');;
var path = require('path')


var Article = require('../models/article');

 var controller = {

    datosCurso: (req, res) =>{
        //console.log("Hola mundo");
        var hola = req.body.hola;

        return res.status(200).send({
            curso: 'Master en Framework JS',
            autor: 'Mauricio Rosas',
            url: 'victorroblesweb.es',
            hola
        });
    },

    test: (req, res) =>{
        return res.status(200).send({
            messaje: 'Soy la accion test de mi controlador de articulos'
        });
    },



    save: (req, res) => {
        //recoger parametros por post
        var params =req.body;

        //Validar datos (validator)
        try {
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
        } catch (err) {
            return res.status(200).send({
                status: 'error',
                messaje: 'faltan datos por enviar !!!'
            });
        }

        if(validate_title && validate_content){
             //Crear el objeto a guardar
             var article = new Article();

             //Asignar valores
             article.title = params.title;
             article.content = params.content;
             article.image = null;
             //Guardar el articulo
             article.save((err, articleStored) => {
                 if(err || !articleStored){
                     return res.status(404).send({
                         status: 'error',
                         messaje: 'El articulo no se a guardado'
                     });
                 }

                 //devolver una respuesta
                 return res.status(200).send({
                     status: 'success',
                     article: articleStored
                 });
            });

            
        }else{
            return res.status(200).send({
                status: 'error',
                messaje: 'los datos no son validos !!!'
            });
        }
       
    },



    getArticles: (req, res) =>{

        var query = Article.find({});

        var last = req.params.last;
        if(last || last != undefined){
            query.limit(5);
        }

        //find
        query.sort('-_id').exec((err, articles) =>{

            if(err){
                return res.status(500).send({
                    status: 'error',
                    messaje: 'Error al devolver los articulos !!!'
                });
            }

            if(!articles){
                return res.status(404).send({
                    status: 'error',
                    messaje: 'no hay articulos para mostrar !!!'
                });
            }

            return res.status(200).send({
                status: 'success',
                articles
            });

        });

    },



    getArticle: (req, res) => {

        //Recoger el id de la url 
        var ArticleId = req.params.id;

        //Comprobar que existe
        if(!ArticleId || ArticleId == null){
            return res.status(404).send({
                status: 'error',
                messaje: 'No existe el articulo !!!'
            });
        }

        //Buscar el articulo
        Article.findById(ArticleId, (err, article) =>{
           
            if (err || !article) {
                return res.status(404).send({
                    status: 'error',
                    messaje: 'No existe el articulo !!!'
                });
            }

            ///Devolverlo en json
            return res.status(200).send({
                status: 'success',
                article
            });

        });
  
    },


    update: (req, res) => {
        //Recoger el id del articulo por la url
        var articleId = req.params.id;

        //Recoger los datos que llegan pot put
        var params = req.body;

        //validar datos
        try {
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
        } catch (err) {
            return res.status(200).send({
                status: 'error',
                messaje: 'Faltan datos por llenar  !!!'
            });
        }

        if(validate_title && validate_content){
            //find and update
            Article.findOneAndUpdate({_id:articleId}, params, {new:true}, (err, articleUpdate) => {
                if (err) {
                    return res.status(500).send({
                        status: 'error',
                        messaje: 'Error al actualizar  !!!'
                    });
                }
                if (!articleUpdate) {
                    return res.status(404).send({
                        status: 'error',
                        messaje: 'No existe el articulo  !!!'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    article: articleUpdate
                });

            });
        }else{
            //devolver respuesta
            return res.status(200).send({
                status: 'error',
                messaje: 'La validacion no es correcta  !!!'
            });
        }

    },


    delete: (req, res) => {
        //Recoger el id dela url
        var articleId = req.params.id;

        //Find and delete
        Article.findOneAndDelete({_id: articleId}, (err, articleRemoved) => {
            if (err) {
                return res.status(500).send({
                    status: 'error',
                    messaje: 'Error al borrar  !!!'
                });
            }

            if (!articleRemoved) {
                return res.status(404).send({
                    status: 'error',
                    messaje: 'No se ha borrado el articulo, posiblemente no exista  !!!'
                });
            }

             //devolver respuesta
            return res.status(200).send({
                status: 'success',
                article:articleRemoved
            });
        });
       
    },

    upload: (req, res) => {
        //configurar el modulo connect  multiparty route/article.js
        
        //Recoger el fichero de la peticion
        var file_name = 'Imagen no subida...';

        if (!req.files) {
            return res.status(200).send({
                status: 'error',
                messaje: file_name
            });
        }

        //conseguir nombre y la extension del archico
        var file_path = req.files.file0.path;
        var file_split = file_path.split('\\');

        //ADVERTENCIA **** EN LINUX O MAC
        //var file_split = file_path.split('/');

        //Nombre del archivo
        var file_name = file_split[2];

        //Entension del fichero
        var extension_split = file_name.split('\.');
        var file_ext = extension_split[1];

        //Comprobar la extension, solo imagenes, si es valida borrar el fichero
        if (file_ext != 'png' && file_ext != 'jpg' && file_ext != 'jpeg' && file_ext != 'gif') {
            //borrar el archivo subido
            fs.unlink(file_path, (err) => {
                return res.status(404).send({
                    status: 'error',
                    messaje: 'La extension de la imagen no es valida  !!!'
                });
            });

        }else{
            //si todo es valido
            var articleId = req.params.id;
            //buscar el articulo, asignarle el nombre de la imagen y actualizarlo
            Article.findOneAndUpdate({_id: articleId}, {image: file_name}, {new:true}, (err, articleUpdate) => {
                //devolver respuesta
                if (err || !articleUpdate) {
                    return res.status(200).send({
                        status: 'success',
                        message: 'Erro al guardar la imagen de articulo'
                    });
                }
                return res.status(200).send({
                    status: 'success',
                    fichero:articleUpdate
                });
            });
            

        }

        
    },// end upload 


    getImage: (req, res) => {
        var file = req.params.image;
        var path_file = './upload/articles/'+file;
        fs.exists(path_file, (exists) => {
            if (exists) {
                return res.sendFile(path.resolve(path_file));
            }else{
                return res.status(200).send({
                    status: 'error',
                    message: 'La imagen no existe !!!'
                });
            }
        });

    },


    search: (req, res) => {
        //Sacar el string a buscar
        var searchString = req.params.search;


        //Find or
        Article.find({ "$or": [
            {"title": {"$regex": searchString, "$options": "i"}},
            {"content": {"$regex": searchString, "$options": "i"}}
        ]})
        .sort([['date', 'descending']])
        .exec((err, articles) => {

            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error en la peticion !!!'
                });
            }

            if (!articles || articles.length <= 0) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay articulos que coincidan con tu busqueda !!!'
                });
            }


            return res.status(200).send({
                status: 'success',
                articles
            });

        });
       
    },
 }; // end controller

 module.exports = controller;